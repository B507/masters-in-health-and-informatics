<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rab');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z>2;imh@? bf1Fu1=3FPGzOvvtS4TJ2!OzOh/HtAa>)BJGrf|*xc:H/}Sn_,T0ao');
define('SECURE_AUTH_KEY',  'sl^.zqE58d*T[Zn)iFHDDqIsK(&~p^AQx;cNPGdLyGAUiTHKg&[.!1C%ehp6vAqE');
define('LOGGED_IN_KEY',    'v7hNgyN`,;!xRtbb#b)&.y}S5cp14Z,S[98%nMq_IuL_bkW<!(1(Ol-b=.GVZuLq');
define('NONCE_KEY',        'u:dQ)zJ7Rh]i>Q^P:0(qDr~}mM0H) c!Oq$9kAIN)yv2LUelB}[=UMqE~`jglb@,');
define('AUTH_SALT',        '<@^A+mKiar@6&U=cap WYk:723?pI|/::{[]Pa^5G<GtJW#n3#8j}BNt2oYy4){}');
define('SECURE_AUTH_SALT', 'c$v`w$JtSa+uzf])ne?/ERVI<lANl8YnL[LB+#O*,nA?aq5uWr&%{>U)G&&9W12o');
define('LOGGED_IN_SALT',   'NG_2]Ss{E3BB!{w#cmfZ-VqeEdm8M6;XKht9!|a>+;q%yFMwoh(6U!YfRFx=8=Jl');
define('NONCE_SALT',       'XI(eb4wvyS>v$<7M`u*2<8zIbykwmcD$*Cypt`fCc=EjuYJj^)3BQ G,ivd=5;CD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
