<?php
$modulus_page_builder = __( 'Page Builder', 'modulus' );
$modulus_page_builder_details = __( 'modulus Pro supports Page Builder. All our shortcodes can be used as widgets too. You can drag and drop our widgets with page builder visual editor.', 'modulus' );
$modulus_page_layout = __( 'Page Layout', 'modulus' );
$modulus_page_layout_details = __( 'modulus Pro offers many different page layouts so you can quickly and easily create your pages with various layout without any hassle!', 'modulus' );
$modulus_unlimited_sidebar = __( 'Unlimited Sidebar', 'modulus' );
$modulus_unlimited_sidebar_details = __( 'Unlimited sidebars allows you to create multiple sidebars. Check out our demo site to see how different pages displays different sidebars!', 'modulus' );
$modulus_shortcode_builder = __( 'Shortcode Builder', 'modulus' );
$modulus_shortcode_builder_details = __( 'With our shortcode builder and lots of shortcodes, you can easily create nested shortcodes and build custom pages!', 'modulus' );
$modulus_portfolio = __( 'Multi Portfolio', 'modulus' );
$modulus_portfolio_details = __( '7 portfolio layouts with Isotope filtering, 3 blog layouts and multiple other alternate layouts for interior pages!', 'modulus' );
$modulus_typography = __( 'Typography', 'modulus' );
$modulus_typography_details = __( 'modulus Pro loves typography, you can choose from over 500+ Google Fonts and Standard Fonts to customize your site!', 'modulus' );
$modulus_slider = __( 'Awesome Sliders', 'modulus' );
$modulus_slider_details = __( 'modulus Pro includes two types of slider. You can use both Flex and Elastic sliders anywhere in your site.', 'modulus' );
$modulus_woocommerce = __( 'Woo Commerce', 'modulus' );
$modulus_woocommerce_details = __( 'modulus Pro has full design/code integration for WooCommerce, your shop will look as good as the rest of your site!', 'modulus' );
$modulus_custom_widget = __( 'Custom Widget', 'modulus' );
$modulus_custom_widget_details = __( 'We offer many custom widgets that are stylized and ready for use. Simply drag &amp; drop into place to activate!', 'modulus' );
$modulus_advanced_admin = __( 'Advanced Admin', 'modulus' );
$modulus_advanced_admin_details = __( ' you can customize any part of your site quickly and easily! using Customizer options.', 'modulus' );
$modulus_font_awesome = __( 'Font Awesome', 'modulus' );
$modulus_font_awesome_details = __( 'Font Awesome icons are fully integrated into the theme. Use them anywhere in your site in 6 different sizes!', 'modulus' );
$modulus_responsive_layout = __( 'Responsive Layout', 'modulus' );
$modulus_responsive_layout_details = __( 'modulus Pro is fully responsive and can adapt to any screen size. Resize your browser window to view it!', 'modulus' );
$modulus_testimonials = __( 'Testimonials', 'modulus' );
$modulus_testimonials_details = __( 'With our testimonial post type, shortcode and widget, Displaying testimonials is a breeze.', 'modulus' );
$modulus_social_media = __( 'Social Media', 'modulus' );
$modulus_social_media_details = __( 'Want your users to stay in touch? No problem, modulus Pro has Social Media icons all throughout the theme!', 'modulus' );
$modulus_google_map = __( 'Google Map', 'modulus' );
$modulus_google_map_details = __( 'modulus Pro includes Goole Map as shortcode and widget. So, you can use it anywhere in your site!', 'modulus' );
$modulus_view_demo = __( 'View Demo', 'modulus');
$modulus_upgrade_to_pro = __( 'Upgrade To Pro', 'modulus' );
$modulus_ask_question = __( 'Ask a Question', 'modulus' );
$modulus_documentation = __( 'Documentation', 'modulus' );

$modulus_why_upgrade = <<< FEATURES
<p class="wrap-header">   
   <a class="ask-question" href="http://www.webulousthemes.com/free-support-request/" target="_blank">
       <i class="fa fa-question-circle"></i> $modulus_ask_question</a>
    <a class="view-doc" href="http://www.webulousthemes.com/modulus-free/" target="_blank">
        <i class="fa fa-server"></i>$modulus_documentation</a>
    <a class="upgrade" href="http://www.webulousthemes.com/theme/modulus-pro" target="_blank">
        <i class="fa fa-upload"></i> $modulus_upgrade_to_pro</a>
    <a class="view-demo" href="http://modulus.webulous.in/" target="_blank">
        <i class="fa fa-eye"></i> $modulus_view_demo</a> 

</p>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-cog"></i></div>
	<h3>$modulus_page_builder</h3>
	<p>$modulus_page_builder_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-th-large"></i></div>
	<h3>$modulus_page_layout</h3>
	<p>$modulus_page_layout_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-th"></i></div>
	<h3>$modulus_unlimited_sidebar</h3>
	<p>$modulus_unlimited_sidebar_details</p>
</div>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-code-fork"></i></div>
	<h3>$modulus_shortcode_builder</h3>
	<p>$modulus_shortcode_builder_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-camera"></i></div>
	<h3>$modulus_portfolio</h3>
	<p>$modulus_portfolio_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-font"></i></div>
	<h3>$modulus_typography</h3>
	<p>$modulus_typography_details</p>
</div>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-slideshare"></i></div>
	<h3>$modulus_slider</h3>
	<p>$modulus_slider_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-leaf"></i></div>
	<h3>$modulus_woocommerce</h3>
	<p>$modulus_woocommerce_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-tasks"></i></div>
	<h3>$modulus_custom_widget</h3>
	<p>$modulus_custom_widget_details</p>
</div>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-dashboard"></i></div>
	<h3>$modulus_advanced_admin</h3>
	<p>$modulus_advanced_admin_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-magic"></i></div>
	<h3>$modulus_font_awesome</h3>
	<p>$modulus_font_awesome_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-arrows"></i></div>
	<h3>$modulus_responsive_layout</h3>
	<p>$modulus_responsive_layout_details</p>
</div>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-magic"></i></div>
	<h3>$modulus_testimonials</h3>
	<p>$modulus_testimonials_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-twitter"></i></div>
	<h3>$modulus_social_media</h3>
	<p>$modulus_social_media_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-map-marker"></i></div>
	<h3>$modulus_google_map</h3>
	<p>$modulus_google_map_details</p>
</div>
FEATURES;

function modulus_theme_page() {
	add_theme_page( 
		__( 'Upgrade To modulus Pro', 'modulus' ),
		__( 'Theme Upgrade', 'modulus' ),
		'edit_theme_options',
		'modulus_upgrade',
		'modulus_display_upgrade'
	);
}

add_action('admin_menu','modulus_theme_page');

function modulus_display_upgrade() {
	global $modulus_why_upgrade;
	echo '<div class="wrap">';
	echo $modulus_why_upgrade;
	echo '</div>';
}