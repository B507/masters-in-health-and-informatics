<?php
/**
 * Internationalization helper.
 *
 * @package     Kirki
 * @category    Core
 * @author      Aristeides Stathopoulos
 * @copyright   Copyright (c) 2016, Aristeides Stathopoulos
 * @license     http://opensource.org/licenses/https://opensource.org/licenses/MIT
 * @since       1.0
 */

if ( ! class_exists( 'Kirki_l10n' ) ) {

	/**
	 * Handles translations
	 */
	class Kirki_l10n {

		/**
		 * The plugin textdomain
		 *
		 * @access protected
		 * @var string
		 */
		protected $textdomain = 'modulus';

		/**
		 * The class constructor.
		 * Adds actions & filters to handle the rest of the methods.
		 *
		 * @access public
		 */
		public function __construct() {

			add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );

		}

		/**
		 * Load the plugin textdomain
		 *
		 * @access public
		 */
		public function load_textdomain() {

			if ( null !== $this->get_path() ) {
				load_textdomain( $this->textdomain, $this->get_path() );
			}
			load_plugin_textdomain( $this->textdomain, false, Kirki::$path . '/languages' );

		}

		/**
		 * Gets the path to a translation file.
		 *
		 * @access protected
		 * @return string Absolute path to the translation file.
		 */
		protected function get_path() {
			$path_found = false;
			$found_path = null;
			foreach ( $this->get_paths() as $path ) {
				if ( $path_found ) {
					continue;
				}
				$path = wp_normalize_path( $path );
				if ( file_exists( $path ) ) {
					$path_found = true;
					$found_path = $path;
				}
			}

			return $found_path;

		}

		/**
		 * Returns an array of paths where translation files may be located.
		 *
		 * @access protected
		 * @return array
		 */
		protected function get_paths() {

			return array(
				WP_LANG_DIR . '/' . $this->textdomain . '-' . get_locale() . '.mo',
				Kirki::$path . '/languages/' . $this->textdomain . '-' . get_locale() . '.mo',
			);

		}

		/**
		 * Shortcut method to get the translation strings
		 *
		 * @static
		 * @access public
		 * @param string $config_id The config ID. See Kirki_Config.
		 * @return array
		 */
		public static function get_strings( $config_id = 'global' ) {

			$translation_strings = array(
				'background-color'      => esc_attr__( 'Background Color', 'modulus' ),
				'background-image'      => esc_attr__( 'Background Image', 'modulus' ),
				'no-repeat'             => esc_attr__( 'No Repeat', 'modulus' ),
				'repeat-all'            => esc_attr__( 'Repeat All', 'modulus' ),
				'repeat-x'              => esc_attr__( 'Repeat Horizontally', 'modulus' ),
				'repeat-y'              => esc_attr__( 'Repeat Vertically', 'modulus' ),
				'inherit'               => esc_attr__( 'Inherit', 'modulus' ),
				'background-repeat'     => esc_attr__( 'Background Repeat', 'modulus' ),
				'cover'                 => esc_attr__( 'Cover', 'modulus' ),
				'contain'               => esc_attr__( 'Contain', 'modulus' ),
				'background-size'       => esc_attr__( 'Background Size', 'modulus' ),
				'fixed'                 => esc_attr__( 'Fixed', 'modulus' ),
				'scroll'                => esc_attr__( 'Scroll', 'modulus' ),
				'background-attachment' => esc_attr__( 'Background Attachment', 'modulus' ),
				'left-top'              => esc_attr__( 'Left Top', 'modulus' ),
				'left-center'           => esc_attr__( 'Left Center', 'modulus' ),
				'left-bottom'           => esc_attr__( 'Left Bottom', 'modulus' ),
				'right-top'             => esc_attr__( 'Right Top', 'modulus' ),
				'right-center'          => esc_attr__( 'Right Center', 'modulus' ),
				'right-bottom'          => esc_attr__( 'Right Bottom', 'modulus' ),
				'center-top'            => esc_attr__( 'Center Top', 'modulus' ),
				'center-center'         => esc_attr__( 'Center Center', 'modulus' ),
				'center-bottom'         => esc_attr__( 'Center Bottom', 'modulus' ),
				'background-position'   => esc_attr__( 'Background Position', 'modulus' ),
				'background-opacity'    => esc_attr__( 'Background Opacity', 'modulus' ),
				'on'                    => esc_attr__( 'ON', 'modulus' ),
				'off'                   => esc_attr__( 'OFF', 'modulus' ),
				'all'                   => esc_attr__( 'All', 'modulus' ),
				'cyrillic'              => esc_attr__( 'Cyrillic', 'modulus' ),
				'cyrillic-ext'          => esc_attr__( 'Cyrillic Extended', 'modulus' ),
				'devanagari'            => esc_attr__( 'Devanagari', 'modulus' ),
				'greek'                 => esc_attr__( 'Greek', 'modulus' ),
				'greek-ext'             => esc_attr__( 'Greek Extended', 'modulus' ),
				'khmer'                 => esc_attr__( 'Khmer', 'modulus' ),
				'latin'                 => esc_attr__( 'Latin', 'modulus' ),
				'latin-ext'             => esc_attr__( 'Latin Extended', 'modulus' ),
				'vietnamese'            => esc_attr__( 'Vietnamese', 'modulus' ),
				'hebrew'                => esc_attr__( 'Hebrew', 'modulus' ),
				'arabic'                => esc_attr__( 'Arabic', 'modulus' ),
				'bengali'               => esc_attr__( 'Bengali', 'modulus' ),
				'gujarati'              => esc_attr__( 'Gujarati', 'modulus' ),
				'tamil'                 => esc_attr__( 'Tamil', 'modulus' ),
				'telugu'                => esc_attr__( 'Telugu', 'modulus' ),
				'thai'                  => esc_attr__( 'Thai', 'modulus' ),
				'serif'                 => _x( 'Serif', 'font style', 'modulus' ),
				'sans-serif'            => _x( 'Sans Serif', 'font style', 'modulus' ),
				'monospace'             => _x( 'Monospace', 'font style', 'modulus' ),
				'font-family'           => esc_attr__( 'Font Family', 'modulus' ),
				'font-size'             => esc_attr__( 'Font Size', 'modulus' ),
				'font-weight'           => esc_attr__( 'Font Weight', 'modulus' ),
				'line-height'           => esc_attr__( 'Line Height', 'modulus' ),
				'font-style'            => esc_attr__( 'Font Style', 'modulus' ),
				'letter-spacing'        => esc_attr__( 'Letter Spacing', 'modulus' ),
				'top'                   => esc_attr__( 'Top', 'modulus' ),
				'bottom'                => esc_attr__( 'Bottom', 'modulus' ),
				'left'                  => esc_attr__( 'Left', 'modulus' ),
				'right'                 => esc_attr__( 'Right', 'modulus' ),
				'center'                => esc_attr__( 'Center', 'modulus' ),
				'justify'               => esc_attr__( 'Justify', 'modulus' ),
				'color'                 => esc_attr__( 'Color', 'modulus' ),
				'add-image'             => esc_attr__( 'Add Image', 'modulus' ),
				'change-image'          => esc_attr__( 'Change Image', 'modulus' ),
				'no-image-selected'     => esc_attr__( 'No Image Selected', 'modulus' ),
				'add-file'              => esc_attr__( 'Add File', 'modulus' ),
				'change-file'           => esc_attr__( 'Change File', 'modulus' ),
				'no-file-selected'      => esc_attr__( 'No File Selected', 'modulus' ),
				'remove'                => esc_attr__( 'Remove', 'modulus' ),
				'select-font-family'    => esc_attr__( 'Select a font-family', 'modulus' ),
				'variant'               => esc_attr__( 'Variant', 'modulus' ),
				'subsets'               => esc_attr__( 'Subset', 'modulus' ),
				'size'                  => esc_attr__( 'Size', 'modulus' ),
				'height'                => esc_attr__( 'Height', 'modulus' ),
				'spacing'               => esc_attr__( 'Spacing', 'modulus' ),
				'ultra-light'           => esc_attr__( 'Ultra-Light 100', 'modulus' ),
				'ultra-light-italic'    => esc_attr__( 'Ultra-Light 100 Italic', 'modulus' ),
				'light'                 => esc_attr__( 'Light 200', 'modulus' ),
				'light-italic'          => esc_attr__( 'Light 200 Italic', 'modulus' ),
				'book'                  => esc_attr__( 'Book 300', 'modulus' ),
				'book-italic'           => esc_attr__( 'Book 300 Italic', 'modulus' ),
				'regular'               => esc_attr__( 'Normal 400', 'modulus' ),
				'italic'                => esc_attr__( 'Normal 400 Italic', 'modulus' ),
				'medium'                => esc_attr__( 'Medium 500', 'modulus' ),
				'medium-italic'         => esc_attr__( 'Medium 500 Italic', 'modulus' ),
				'semi-bold'             => esc_attr__( 'Semi-Bold 600', 'modulus' ),
				'semi-bold-italic'      => esc_attr__( 'Semi-Bold 600 Italic', 'modulus' ),
				'bold'                  => esc_attr__( 'Bold 700', 'modulus' ),
				'bold-italic'           => esc_attr__( 'Bold 700 Italic', 'modulus' ),
				'extra-bold'            => esc_attr__( 'Extra-Bold 800', 'modulus' ),
				'extra-bold-italic'     => esc_attr__( 'Extra-Bold 800 Italic', 'modulus' ),
				'ultra-bold'            => esc_attr__( 'Ultra-Bold 900', 'modulus' ),
				'ultra-bold-italic'     => esc_attr__( 'Ultra-Bold 900 Italic', 'modulus' ),
				'invalid-value'         => esc_attr__( 'Invalid Value', 'modulus' ),
				'add-new'           	=> esc_attr__( 'Add new', 'modulus' ),
				'row'           		=> esc_attr__( 'row', 'modulus' ),
				'limit-rows'            => esc_attr__( 'Limit: %s rows', 'modulus' ),
				'open-section'          => esc_attr__( 'Press return or enter to open this section', 'modulus' ),
				'back'                  => esc_attr__( 'Back', 'modulus' ),
				'reset-with-icon'       => sprintf( esc_attr__( '%s Reset', 'modulus' ), '<span class="dashicons dashicons-image-rotate"></span>' ),
				'text-align'            => esc_attr__( 'Text Align', 'modulus' ),
				'text-transform'        => esc_attr__( 'Text Transform', 'modulus' ),
				'none'                  => esc_attr__( 'None', 'modulus' ),
				'capitalize'            => esc_attr__( 'Capitalize', 'modulus' ),
				'uppercase'             => esc_attr__( 'Uppercase', 'modulus' ),
				'lowercase'             => esc_attr__( 'Lowercase', 'modulus' ),
				'initial'               => esc_attr__( 'Initial', 'modulus' ),
				'select-page'           => esc_attr__( 'Select a Page', 'modulus' ),
				'open-editor'           => esc_attr__( 'Open Editor', 'modulus' ),
				'close-editor'          => esc_attr__( 'Close Editor', 'modulus' ),
				'switch-editor'         => esc_attr__( 'Switch Editor', 'modulus' ),
				'hex-value'             => esc_attr__( 'Hex Value', 'modulus' ),
			);

			$config = apply_filters( 'kirki/config', array() );

			if ( isset( $config['i18n'] ) ) {
				$translation_strings = wp_parse_args( $config['i18n'], $translation_strings );
			}

			return apply_filters( 'kirki/' . $config_id . '/l10n', $translation_strings );

		}
	}
}
